﻿using UnityEngine;
using System.Collections;

public class levelAnimationController : MonoBehaviour {

    public float maxSpeed = 10;
    public float smoothTime = 0.3f;
    private float speed = 0;
    private float currentVelocity = 0;

    public Animator soldierAnimator;

    private Transform myTransform;

    void Awake()
    {
        myTransform = transform;
    }

    void Update ()
    {
        speed = Input.GetAxis("Horizontal") > 0.1
        ? Mathf.SmoothDamp(speed, maxSpeed, ref currentVelocity, smoothTime)
        : Mathf.SmoothDamp(speed, 0, ref currentVelocity, smoothTime);

        myTransform.position += Vector3.right * speed * Time.deltaTime;

        soldierAnimator.SetFloat("Speed", speed);
    }
}
