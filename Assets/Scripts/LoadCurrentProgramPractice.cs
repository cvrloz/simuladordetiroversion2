﻿using UnityEngine;
using System.Collections;
using Scripts;

namespace Assets.Scripts.practices
{
	public class LoadCurrentProgramPractice : MonoBehaviour
	{
		private JSONObject results;

		// Use this for initialization
		void OnEnable()
		{
			load();
		}

		public void load()
		{
			StartCoroutine(getLessons());
		}

		IEnumerator getLessons()
		{
			if (UserInstance.account == null)
			{
				yield return null;
			}
			else
			{
				HTTP.Request someRequest = new HTTP.Request("get",
					VARS.BASE_API_URL + "ProgramPractice/current/?user=" + UserInstance.account.id);
				someRequest.Send();
				someRequest.Send((request) =>
				{
					if (request.response.status == 200 || request.response.status == 201)
					{
						UserInstance.setCurrentProgramPractice(request.response.Text);
						if (UserInstance.currentProgramPractice != null)
						{
							UserInstance.setCurrentProgramPractice(request.response.Text);
						}
					}
					else
					{
						//StartCoroutine(warning("VERIFIQUE LA CONEXIÓN A INTERNET--"));
					}
				});
				yield return null;
			}
		}
	}
}