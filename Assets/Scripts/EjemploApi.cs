﻿using System.Collections;
using Scripts;
using UnityEngine;

public class EjemploApi : MonoBehaviour
{
	private Hashtable data;

	private HTTP.Request theRequest;

	void Start()
	{
		data = new Hashtable();
		data.Add("username", "9981765");
		data.Add("password", "9981765");

		theRequest = new HTTP.Request("post", VARS.BASE_API_URL + "Login/", data);
		theRequest.synchronous = true;
		theRequest.Send(
			(request) =>
			{
				/**
                    # Formato de respuesta Login
                    token: "3bd1af94492661e3459f3f51a65ce0d20226631e"
                    user: {
                        email: "q@q.com"
                        id: 1
                        image: null
                        info: 1
                        is_active: true
                        is_admin: true
                        is_staff: false
                        is_superuser: false
                        password: "pbkdf2_sha256$20000$ZUe6dWQkT1cL$J3wBBF+0AKU4cGljWN02JPunvN9ktj/oqdJWaPVKNhg="
                        phone_number: ""
                        username: "q"
                    }
                    **/
				Hashtable result = request.response.Object;
				if (result != null && (request.response.status == 200 || request.response.status == 201))
				{
					Debug.Log(request.response.status);
					Debug.Log("Obtenido correctamente");
					Debug.Log(result["token"]);
					Hashtable subObjectUser = (Hashtable) result["user"];
					Debug.Log(subObjectUser["email"]);
					Debug.Log(result);
				}
				else if (request.response.status == 401)
				{
					Debug.LogWarning(result["detail"]);
				}
				else
				{
					Debug.LogWarning("Could not parse JSON response!");
					return;
				}
			});
	}

	// Update is called once per frame
	void Update()
	{
	}
}