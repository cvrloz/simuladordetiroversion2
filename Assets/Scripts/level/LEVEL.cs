﻿using Assets.Scripts.serializers;

namespace Assets.Scripts.level
{
	class LEVEL
	{
		public static Lesson[] listCurrentLessons = null;
		public static bool is_custom_lesson = false;
		public static int indexCurrentLesson = -1;
		public static int indexCurrentTypeOfFire = -1;

		public static Practices practices = null;

		public static void setCurrentLessons(Lesson[] _listCurrentLessons, bool _is_custom_lesson)
		{
			listCurrentLessons = _listCurrentLessons;
			is_custom_lesson = _is_custom_lesson;
			indexCurrentLesson = 0;
			indexCurrentTypeOfFire = 0;

			if (!is_custom_lesson)
			{
				practices = new Practices(UserInstance.account, UserInstance.currentProgramPractice);
			}
			else
			{
				practices = new Practices(UserInstance.account);
			}
		}

		public static Lesson[] getCurrentLessons()
		{
			return listCurrentLessons;
		}

		public static Lesson[] getDefaultLessons()
		{
			Lesson[] customLesson = new Lesson[1];
			customLesson[0] = (new Lesson()).customLesson(15, 10, (int) (2*60), 5);
			return customLesson;
		}

		public static string getPracticeJson()
		{
			JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
			request.AddField("practicing", practices.practicing.id);
			JSONObject results = new JSONObject(JSONObject.Type.ARRAY);

			for (int i = 0; i < practices.results.Count; i++)
			{
				JSONObject result = new JSONObject(JSONObject.Type.OBJECT);
				result.AddField("lesson", practices.results[i].lesson.id);
				result.AddField("type_of_fire", practices.results[i].type_of_fire.id);
				result.AddField("position", practices.results[i].position.id);
				JSONObject results_zones = new JSONObject(JSONObject.Type.ARRAY);
				for (int j = 0; j < practices.results[i].results_zone.Length; j++)
				{
					JSONObject results_zone = new JSONObject(JSONObject.Type.OBJECT);
					results_zone.AddField("zone", practices.results[i].results_zone[j].zone);
					results_zone.AddField("time", practices.results[i].results_zone[j].time);
					results_zone.AddField("score", practices.results[i].results_zone[j].score);
					results_zones.Add(results_zone);
				}
				result.AddField("results_zone", results_zones);
				results.Add(result);
			}

			request.AddField("results", results);
			if (!is_custom_lesson)
			{
				request.AddField("program_practice", practices.program_practice.id);
			}
			return request.ToString();
		}

		public static void clear()
		{
			listCurrentLessons = null;
			indexCurrentTypeOfFire = -1;
			indexCurrentLesson = -1;
			is_custom_lesson = false;
		}
	}
}