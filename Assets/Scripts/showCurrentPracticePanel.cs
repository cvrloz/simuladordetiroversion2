﻿using Assets.Scripts.serializers;
using UnityEngine;
using UnityEngine.UI;

class showCurrentPracticePanel : MonoBehaviour
{
	public RectTransform listCurrentPracticePanel;
	public RectTransform detailCurrentPracticePanel;

	void OnEnable()
	{
		generateList();
	}

	public void generateList()
	{
		if (UserInstance.currentProgramPractice != null)
		{
			string name = UserInstance.currentProgramPractice.title;
			GameObject button = (GameObject) Instantiate(Resources.Load("Button1"));
			button.GetComponent<Image>().color = new Color(0, 0, 0, 1);
			button.transform.SetParent(listCurrentPracticePanel, false);
			Button b = button.GetComponent<Button>();
			Text t = b.GetComponentInChildren<Text>();
			t.GetComponent<Text>().color = new Color(1, 1, 1);
			t.text = name;
			b.onClick.AddListener(() => OnClickCurrentPractice(UserInstance.currentProgramPractice));
		}
	}

	void OnClickCurrentPractice(CurrentProgramPractice current)
	{
		clearDetailCurrentPracticePanel();
		CurrentProgramPractice cpp = UserInstance.currentProgramPractice;
		Account instructor = cpp.instructor;
		Lesson[] lessons = cpp.lesson;
		string detail = "Instructor: " + instructor.military_grade._short + ". " + instructor.first_name + " " +
		                instructor.last_name + "\n";
		detail += "Lecciones de tiro: \n";
		for (int i = 0; i < lessons.Length; i++)
		{
			detail += "     -" + lessons[i].name + "\n";
		}
		detail += "Fecha de inicio: \n\t\t" + cpp.start.ToString() + "\n";
		detail += "Fecha de finalizacion: \n\t\t" + cpp.end.ToString();
		Text text = detailCurrentPracticePanel.FindChild("description").GetComponent<Text>();
		text.text = detail;
		GameObject button = detailCurrentPracticePanel.FindChild("ButtonStart").gameObject;
		button.SetActive(true);
		Button buttonStart = button.GetComponent<Button>();
		buttonStart.onClick.RemoveAllListeners();
		buttonStart.onClick.AddListener(() => OnClickCurrentPractice(cpp));
	}

	void OnClickStartCurrentPractice(CurrentProgramPractice current)
	{
	}

	void OnDisable()
	{
		foreach (Transform childTransform in listCurrentPracticePanel.transform)
		{
			Destroy(childTransform.gameObject);
		}
		clearDetailCurrentPracticePanel();
	}

	void clearDetailCurrentPracticePanel()
	{
		foreach (Transform childTransform in detailCurrentPracticePanel.transform)
		{
			// Destroy(childTransform.gameObject);
		}
	}
}