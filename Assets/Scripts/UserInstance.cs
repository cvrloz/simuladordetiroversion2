﻿using System;
using UnityEngine;
using Assets.Scripts.serializers;

public class UserInstance
{
	public static bool logged = false;
	public static Account account = null;

	public static CurrentProgramPractice currentProgramPractice = null;
	public static string token = null;


	public static void setLogin(bool value)
	{
		logged = value;
	}

	public static bool isLogged()
	{
		return account != null;
	}

	public static void printAll()
	{
		if (account != null)
		{
			Debug.Log(account.first_name);
			Debug.Log(account.last_name);
			Debug.Log(account.ci);
			Debug.Log(account.username);
		}
	}

	public static void set(JSONObject jsonObject)
	{
		token = (string) jsonObject.GetField("token").str;
		account = new Account(jsonObject.GetField("user"));
	}

	public static void setCurrentProgramPractice(string json)
	{
		CurrentProgramPractice cpp = new CurrentProgramPractice(json);
		if (cpp.id == 0)
		{
			currentProgramPractice = null;
		}
		else
		{
			currentProgramPractice = cpp;
		}
	}

	public static String getFullName()
	{
		if (account != null)
		{
			return account.military_grade._short + ". " + account.first_name + " " + account.last_name;
		}
		return "";
	}
}