﻿using UnityEngine;
using UnityEngine.UI;

public class BackgroundSong : MonoBehaviour
{
	public GameObject camera;
	public Toggle muteToogle;
	private AudioSource a;

	// Use this for initialization
	void Start()
	{
		a = camera.GetComponent<AudioSource>();
	}

	public void changeValue(bool mute)
	{
		a.mute = !mute;
	}
}