﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.serializers
{
	public class Practices
	{
		private JSONObject practices;

		public int id;
		public Account practicing;
		public List<Results> results;
		public DateTime date_practice;
		public CurrentProgramPractice program_practice;

		public Practices()
		{
			this.id = -1;
		}

		public Practices(Account practicig)
		{
			this.id = -1;
			this.practicing = practicig;
			this.results = new List<Results>();
			this.program_practice = null;
		}

		public Practices(Account practicig, CurrentProgramPractice program_practice)
		{
			this.id = -1;
			this.practicing = practicig;
			this.results = new List<Results>();
			this.program_practice = program_practice;
		}

		public void addResult(Lesson lesson, TypeOfFire type_of_fire, Position position, List<ResultsZone> results_zone)
		{
			this.results.Add(new Results(lesson, type_of_fire, position, results_zone));
		}

		public Practices(int Practicig, Results results, int program_practice)
		{
			practices = new JSONObject(JSONObject.Type.OBJECT);
			practices.AddField("practicing", Practicig);
			practices.AddField("resutls", results.getObjectJson());
			practices.AddField("program_practice", program_practice);
		}

		public string GetJsonString()
		{
			return practices.ToString();
		}
	}
}