﻿namespace Assets.Scripts.serializers
{
	public class Practice
	{
		public int lesson { get; set; }
		public int type_of_fire { get; set; }
		public int position { get; set; }
		public ResultsZone[] results_zone { get; set; }

		public Practice(int lesson, int type_of_fire, int position, ResultsZone[] results_zone)
		{
			this.lesson = lesson;
			this.type_of_fire = type_of_fire;
			this.position = position;
			this.results_zone = results_zone;
		}
	}
}