﻿namespace Assets.Scripts.serializers
{
	public class ResultsZone
	{
		public int zone { get; set; }
		public int time { get; set; }
		public int score { get; set; }

		public ResultsZone(int zone, int time, int score)
		{
			this.zone = zone;
			this.time = time;
			this.score = score;
		}
	}
}