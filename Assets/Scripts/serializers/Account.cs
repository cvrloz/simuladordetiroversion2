namespace Assets.Scripts.serializers
{
	public class Account
	{
		public int id;
		public string username;
		public UserType user_type;
		public string email;
		public string image;
		public string phone_number;
		public bool gender;
		public int ci;
		public string first_name;
		public string last_name;
		public int date_of_birth;
		public City city;
		public MilitaryGrade military_grade;

		public Account(JSONObject jsonObject)
		{
			this.id = (int) jsonObject.GetField("id").n;
			this.username = (string) jsonObject.GetField("username").str;
			this.email = (string) jsonObject.GetField("email").str;
			this.image = (string) jsonObject.GetField("image").str;
			this.gender = (bool) jsonObject.GetField("gender").b;
			this.ci = (int) jsonObject.GetField("ci").n;
			this.first_name = (string) jsonObject.GetField("first_name").str;
			this.last_name = (string) jsonObject.GetField("last_name").str;

			this.user_type = new UserType(jsonObject.GetField("user_type"));
			this.military_grade = new MilitaryGrade(jsonObject.GetField("military_grade"));
			this.city = new City(jsonObject.GetField("military_grade"));
			//this.date_of_birth = (int) _json_object.GetField("date_of_birth").n;
		}

		public string getFullName()
		{
			return military_grade._short + ". " + first_name + " " + last_name;
		}
	}
}