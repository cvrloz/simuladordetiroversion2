﻿namespace Assets.Scripts.serializers
{
	public class Position
	{
		public static int PARADO = 1;
		public static int TENDIDO = 2;
		public static int AGAZAPADO = 3;
		public static int ARRODILLADO = 4;

		public int id;
		public string name;
		public string description;
		public string image;

		public Position()
		{
		}

		public Position(JSONObject jsonObject)
		{
			this.id = (int) jsonObject.GetField("id").n;
			this.name = (string) jsonObject.GetField("name").str;
			this.description = (string) jsonObject.GetField("description").str;
			this.image = (string) jsonObject.GetField("image").str;
		}

		public Position customPosition()
		{
			Position position = new Position();
			position.id = -1;
			position.name = "De pie";
			position.description = "";
			position.image = "";
			return position;
		}

		protected bool Equals(Position other)
		{
			throw new System.NotImplementedException();
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Position) obj);
		}

		public override int GetHashCode()
		{
			throw new System.NotImplementedException();
		}

		public static int Parado
		{
			get { return PARADO; }
		}

		public static int Tendido
		{
			get { return TENDIDO; }
		}

		public static int Agazapado
		{
			get { return AGAZAPADO; }
		}

		public static int Arrodillado
		{
			get { return ARRODILLADO; }
		}
	}
}