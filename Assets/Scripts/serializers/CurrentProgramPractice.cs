using UnityEngine;
using System;

namespace Assets.Scripts.serializers
{
	public class CurrentProgramPractice
	{
		public int id = 0;
		public string title;
		public Account instructor;
		public Lesson[] lesson;
		public DateTime start;
		public DateTime end;
		public bool is_evaluation;
		public bool is_test_mode;
		public bool finish;
		public bool completed;

		public CurrentProgramPractice(string jsonObject)
		{
			JSONObject json = new JSONObject(jsonObject);
			JSONObject practice = (JSONObject) json.list[0]; // get list array program practice
			if (practice.Count > 0)
			{
				JSONObject current = (JSONObject) practice.list[0];
				this.id = (int) current.GetField("id").n;
				this.title = (string) current.GetField("title").str;
				this.is_evaluation = (bool) current.GetField("is_evaluation").b;
				this.completed = (bool) current.GetField("completed").b;
				this.is_test_mode = (bool) current.GetField("is_test_mode").b;
				this.finish = (bool) current.GetField("finish").b;
				this.start = DateTime.Parse((string) current.GetField("start").str);
				this.end = DateTime.Parse((string) current.GetField("end").str);
				this.instructor = new Account((JSONObject) current.GetField("instructor"));
				JSONObject lesson_array = current.GetField("lesson");
				this.lesson = new Lesson[lesson_array.Count];
				for (int i = 0; i < this.lesson.Length; i++)
				{
					this.lesson[i] = new Lesson(lesson_array[i]);
				}
			}
			else
			{
				Debug.Log("No tiene practicas pendientes");
			}
		}
	}
}