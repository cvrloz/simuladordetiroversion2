﻿namespace Assets.Scripts.serializers
{
	public class Lesson
	{
		public int id;
		public string name;
		public string description;
		public string image;
		public TypeOfFire[] type_of_fire;

		public Lesson()
		{
		}

		public Lesson(JSONObject jsonObject)
		{
			this.id = (int) jsonObject.GetField("id").n;
			this.name = (string) jsonObject.GetField("name").str;
			this.description = (string) jsonObject.GetField("description").str;
			this.image = (string) jsonObject.GetField("image").str;
			JSONObject type_of_fire_array = jsonObject.GetField("type_of_fire");
			this.type_of_fire = new TypeOfFire[type_of_fire_array.Count];
			for (int i = 0; i < this.type_of_fire.Length; i++)
			{
				this.type_of_fire[i] = new TypeOfFire(type_of_fire_array[i]);
			}
		}

		public Lesson customLesson(int distance, int cartridges, int max_time, int min_score)
		{
			Lesson custom = new Lesson();
			custom.id = -1;
			custom.name = "Practica customizada";
			custom.description = "";
			custom.image = "";
			TypeOfFire[] type_of_fire = new TypeOfFire[1];
			type_of_fire[0] = (new TypeOfFire()).customTypeOfFire(distance, cartridges, max_time, min_score);
			custom.type_of_fire = type_of_fire;
			return custom;
		}
	}
}