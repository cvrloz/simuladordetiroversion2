﻿using UnityEngine;

public class TimerTask : MonoBehaviour
{
//review 1
	private int seconds;
	private float secondsPartial;
	private bool hasFinished;

	public TimerTask(int seconds) : base()
	{
		this.seconds = seconds;
		this.secondsPartial = seconds;
		hasFinished = false;
		Debug.Log("--dsdsdd--");
	}

	void Start()
	{
	}

	void Update()
	{
		Debug.Log("--");
		seconds = (int) secondsPartial;
		if (secondsPartial > 0 && hasFinished)
		{
			secondsPartial -= Time.deltaTime;
		}
		if (secondsPartial < 1)
		{
			hasFinished = true;
		}
	}

	public bool hasFinishedTimer()
	{
		return seconds > 0;
	}

	public int getTime()
	{
		return (int) secondsPartial;
	}

	public string getTimeString()
	{
		return string.Format("{0:00}:{1:00}:{2:00}", seconds/3600, (seconds/60)%60, seconds%60);
	}

	public string getTimeString(string format)
	{
		return string.Format(format, seconds/3600, (seconds/60)%60, seconds%60);
	}
}