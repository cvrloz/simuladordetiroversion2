﻿namespace Assets.Scripts.serializers
{
	public class Target
	{
		public int id;
		public string name;
		public string description;
		public string image;
		public int zones;

		public Target()
		{
		}

		public Target(JSONObject jsonObject)
		{
			this.id = (int) jsonObject.GetField("id").n;
			this.name = (string) jsonObject.GetField("name").str;
			this.description = (string) jsonObject.GetField("description").str;
			this.image = (string) jsonObject.GetField("image").str;
			this.zones = (int) jsonObject.GetField("zones").n;
		}

		public Target customTarget()
		{
			Target target = new Target();
			target.id = 1;
			target.name = "Blaco tipo A";
			target.description = "";
			target.image = "";
			target.zones = 10;
			return target;
		}
	}
}