﻿namespace Assets.Scripts.serializers
{
	public class UserType
	{
		public int id;
		public string name;
		public string _short;

		public UserType(JSONObject jsonObject)
		{
			this.id = (int) jsonObject.GetField("id").n;
			this.name = (string) jsonObject.GetField("name").str;
			this._short = (string) jsonObject.GetField("short").str;
		}
	}
}