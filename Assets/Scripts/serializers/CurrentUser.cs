﻿namespace Assets.Scripts.serializers
{
	public class CurrentUser
	{
		public string token;
		public Account user;

		public CurrentUser(JSONObject jsonObject)
		{
			this.token = jsonObject.GetField("token").str;
			this.user = new Account(jsonObject.GetField("user"));
		}
	}
}