using System;

namespace Assets.Scripts.serializers
{
	public class ProgramPractice
	{
		public int id;
		public string title;
		public Account instructor;
		public Lesson[] lesson;
		public DateTime start;
		public DateTime end;
		public bool is_evaluation;
		public bool is_test_mode;
		public bool finish;
		public bool completed;
	}
}