﻿using System.Collections.Generic;

namespace Assets.Scripts.serializers
{
	public class Results
	{
		private JSONObject results_array;

		public Lesson lesson;
		public TypeOfFire type_of_fire;
		public Position position;
		public ResultsZone[] results_zone;

		public Results()
		{
			results_array = new JSONObject(JSONObject.Type.ARRAY);
		}

		public Results(Lesson lesson, TypeOfFire type_of_fire, Position position, List<ResultsZone> results_zones)
		{
			this.lesson = lesson;
			this.type_of_fire = type_of_fire;
			this.position = position;
			this.results_zone = new ResultsZone[results_zones.Count];
			for (int i = 0; i < results_zones.Count; i++)
			{
				this.results_zone[i] = results_zones[i];
			}
		}

		public Results(Practice practice)
		{
			results_array = new JSONObject(JSONObject.Type.ARRAY);
			/* START - Generate result zone array Json*/
			JSONObject result_zones_json = new JSONObject(JSONObject.Type.ARRAY);
			for (int i = 0; i < practice.results_zone.Length; i++)
			{
				JSONObject result = new JSONObject(JSONObject.Type.OBJECT);
				result.AddField("zone", practice.results_zone[i].zone);
				result.AddField("time", practice.results_zone[i].time);
				result.AddField("score", practice.results_zone[i].score);
				result_zones_json.Add(result);
			}
			/* END - Generate result zone array Json*/
			JSONObject practice_json = new JSONObject(JSONObject.Type.OBJECT);
			practice_json.AddField("lesson", practice.lesson);
			practice_json.AddField("type_of_fire", practice.type_of_fire);
			practice_json.AddField("position", practice.position);
			practice_json.AddField("results_zone", result_zones_json);
			results_array.Add(practice_json);
		}

		public void add_response(Practice practice)
		{
			/* START - Generate result zone array Json*/
			JSONObject result_zones_json = new JSONObject(JSONObject.Type.ARRAY);
			for (int i = 0; i < practice.results_zone.Length; i++)
			{
				JSONObject result = new JSONObject(JSONObject.Type.OBJECT);
				result.AddField("zone", practice.results_zone[i].zone);
				result.AddField("time", practice.results_zone[i].time);
				result.AddField("score", practice.results_zone[i].score);
				result_zones_json.Add(result);
			}
			/* END - Generate result zone array Json*/
			JSONObject practice_json = new JSONObject(JSONObject.Type.OBJECT);
			practice_json.AddField("lesson", practice.lesson);
			practice_json.AddField("type_of_fire", practice.type_of_fire);
			practice_json.AddField("position", practice.position);
			practice_json.AddField("results_zone", result_zones_json);
			results_array.Add(practice_json);
		}

		public JSONObject getObjectJson()
		{
			return results_array;
		}

		public string GetJsonString()
		{
			return results_array.ToString();
		}
	}
}