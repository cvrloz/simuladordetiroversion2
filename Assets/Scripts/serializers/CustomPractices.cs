﻿using System;

namespace Assets.Scripts.serializers
{
	public class CustomPractices
	{
		private JSONObject customPractices;

		public int id;
		public Account practicing;
		public Results[] results;
		public DateTime date_practice;
		public ProgramPractice program_practice;

		public CustomPractices()
		{
			this.id = -1;
		}

		public CustomPractices(int Practicig, Results results)
		{
			customPractices = new JSONObject(JSONObject.Type.OBJECT);
			customPractices.AddField("practicing", Practicig);
			customPractices.AddField("resutls", results.getObjectJson());
		}

		public string GetJsonString()
		{
			return customPractices.ToString();
		}
	}
}