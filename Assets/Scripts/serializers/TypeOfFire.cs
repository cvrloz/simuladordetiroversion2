﻿namespace Assets.Scripts.serializers
{
	public class TypeOfFire
	{
		public int id;
		public string name;
		public Position position;
		public float distance;
		public string selector;
		public Target target;
		public int chargers;
		public int cartridges;
		public string modality;
		public int max_time;
		public int min_score;
		public int max_score;

		public TypeOfFire()
		{
		}

		public TypeOfFire(JSONObject jsonObject)
		{
			this.id = (int) jsonObject.GetField("id").n;
			this.name = (string) jsonObject.GetField("name").str;
			this.position = new Position(jsonObject.GetField("position"));
			this.distance = (float) jsonObject.GetField("distance").f;
			this.selector = (string) jsonObject.GetField("selector").str;
			this.target = new Target(jsonObject.GetField("target"));
			this.chargers = (int) jsonObject.GetField("chargers").n;
			this.cartridges = (int) jsonObject.GetField("cartridges").n;
			this.modality = (string) jsonObject.GetField("modality").str;
			this.max_time = (int) jsonObject.GetField("max_time").n;
			this.min_score = (int) jsonObject.GetField("min_score").n;
			this.max_score = (int) jsonObject.GetField("max_score").n;
		}

		public TypeOfFire customTypeOfFire(int distance, int cartridges, int max_time, int min_score)
		{
			TypeOfFire type_of_fire = new TypeOfFire();
			type_of_fire.id = -1;
			type_of_fire.name = "Fuego customizado";
			type_of_fire.position = (new Position()).customPosition();
			type_of_fire.distance = distance;
			type_of_fire.selector = "";
			type_of_fire.target = (new Target()).customTarget();
			type_of_fire.chargers = 1;
			type_of_fire.cartridges = cartridges;
			type_of_fire.modality = "";
			type_of_fire.max_time = max_time;
			type_of_fire.min_score = min_score;
			type_of_fire.max_score = 1000;
			return type_of_fire;
		}
	}
}