﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.level;
using Assets.Scripts.serializers;
using UnityEngine.UI;

public class CustomizeLesson : MonoBehaviour
{
	public Slider timeSlider, ammoSlider, minscoreSlider, distanceSlider;
	public Text timeText, ammoText, minscoreText, distanceText;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		updateReferences();
	}

	void updateReferences()
	{
		timeText.text = timeSlider.value.ToString();
		ammoText.text = ammoSlider.value.ToString();
		minscoreText.text = minscoreSlider.value.ToString();
		distanceText.text = distanceSlider.value.ToString();
	}

	public void newLesson()
	{
		int ammoInt = (int) ammoSlider.value;
		int distanceInt = (int) distanceSlider.value;
		int minscoreInt = (int) minscoreSlider.value;

		Lesson[] customLesson = new Lesson[1];
		customLesson[0] = (new Lesson()).customLesson(distanceInt, ammoInt, (int) (timeSlider.value*60), minscoreInt);

		LEVEL.setCurrentLessons(customLesson, true);
		LEVEL.indexCurrentTypeOfFire = 0;
		LEVEL.indexCurrentLesson = 0;
		LEVEL.is_custom_lesson = true;

		Application.LoadLevel(1);
	}
}