﻿using UnityEngine;
using System.Collections;

public class TargetAnim : MonoBehaviour {

    public Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey("a"))
        {
            anim.SetInteger("direction", 4);
        }
        else if (Input.GetKey("s"))
        {
            anim.SetInteger("direction", 3);
        }
        else if (Input.GetKey("d"))
        {
            anim.SetInteger("direction", 2);
        }
        else if (Input.GetKey("w"))
        {
            anim.SetInteger("direction", 1);
        }
        else 
        {
            anim.SetInteger("direction", 0);
        }
        


	
	}
}
