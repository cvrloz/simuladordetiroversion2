﻿using UnityEngine;

public class Desviation
{
	float x;
	float y;

	public Desviation(float x, float y)
	{
		float prox = Random.Range(0f, 1f);
		float proy = Random.Range(0f, 1f);
		Debug.Log(prox);
		Debug.Log(proy);
		if (prox < 0.2f)
		{
			//+2mm
			this.x = x + 0.2f;
		}
		else if (prox > 0.2f && prox < 0.275)
		{
			// + 1mm
			this.x = x + 0.1f;
		}
		else
		{
			//centro
			this.x = x;
		}
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}
}