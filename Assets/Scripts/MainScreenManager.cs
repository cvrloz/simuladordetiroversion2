﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.level;
using Assets.Scripts.serializers;
using Assets.Scripts.settings;
using Assets.Scripts.toast;
using Scripts;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainScreenManager : MonoBehaviour
{
	public GameObject newPracticePanel;
	public GameObject mainPanel;
	public GameObject loginPanel;
	public GameObject customizePanel;
	public GameObject aboutPanel;
	public GameObject currentPracticesPanel;
	public GameObject messagePanel;
	public GameObject configurationPanel;
	public GameObject btnCurrentPractice;
	public InputField inputUsername, inputPassword;
	public InputField username, password;	

	public GameObject panels;

	private HTTP.Request theRequest;

	private Toast _toast;

    void Awake()
    {
        Application.targetFrameRate = 30;
    }

	void Start()
	{
		newPracticePanel.SetActive(false);
		mainPanel.SetActive(false);
		loginPanel.SetActive(false);
		customizePanel.SetActive(false);
		aboutPanel.SetActive(false);
		_toast = new Toast(messagePanel);
		if (UserInstance.isLogged())
		{
			mainPanel.SetActive(true);
			panels.SetActive(true);
		}
		else
		{
			loginPanel.SetActive(true);
			panels.SetActive(false);
		}
	}

	void Update()
	{
		_toast.Update();
	}

	public void login()
	{
		StartCoroutine(makeLogin());
	}

	IEnumerator makeLogin()
	{
        WWWForm form = new WWWForm();
		form.AddField("username", username.text);
		form.AddField("password", password.text);
		_toast.warning("Espere...");
		HTTP.Request someRequest = new HTTP.Request("post", VARS.BASE_API_URL + "Login/", form);

		someRequest.Send((request) =>
		{
			bool result = false;
			Hashtable thing = (Hashtable) JSON.JsonDecode(request.response.Text, ref result);
			if (result != null && (request.response.status == 200 || request.response.status == 201))
			{
				panels.SetActive(true);
				loginPanel.SetActive(false);
				mainPanel.SetActive(true);
				UserInstance.set(new JSONObject(request.response.Text));
				_toast.success("BIENVENIDO: " + UserInstance.getFullName());
				StartCoroutine(loadProgramPractices());
			}
			else if (!result)
			{
				_toast.error("VERIFIQUE LA CONEXIÓN A INTERNET");
				return;
			}
			else
			{
				_toast.error("CREDENCIALES INVÁLIDAS");
			}
		});
		yield return null;
	}


	public void newPractice()
	{
		mainPanel.SetActive(false);
		newPracticePanel.SetActive(true);
	}

	public void backToMainFromAbout()
	{
		mainPanel.SetActive(true);
		aboutPanel.SetActive(false);
	}

	public void about()
	{
		mainPanel.SetActive(false);
		aboutPanel.SetActive(true);
	}

	public void backToMain()
	{
		mainPanel.SetActive(true);
		customizePanel.SetActive(false);
		aboutPanel.SetActive(false);
		currentPracticesPanel.SetActive(false);
		newPracticePanel.SetActive(false);
		configurationPanel.SetActive(false);
	}

	public void backToMainFromCustom()
	{
		customizePanel.SetActive(false);
		mainPanel.SetActive(true);
	}


	public void customizePractice()
	{
		mainPanel.SetActive(false);
		customizePanel.SetActive(true);
	}

	public void isLogged(int a)
	{
		if (a == 1)
		{
			loginPanel.SetActive(false);
			mainPanel.SetActive(true);
		}
		else
		{
			newPracticePanel.SetActive(false);
			loginPanel.SetActive(true);
		}
	}

	public void calibrate()
	{
		SceneManager.LoadScene("Calibration");
	}

	public void resetToDefault()
	{
		PlayerPrefs.SetFloat("x", 1f);
		PlayerPrefs.SetFloat("y", 1f);
		_toast.info("Calibracion por defecto");
	}

	public void configuration()
	{
		mainPanel.SetActive(false);
		configurationPanel.SetActive(true);
	}


	IEnumerator loadProgramPractices()
	{
		HTTP.Request someRequest = new HTTP.Request("get",
			VARS.BASE_API_URL + "ProgramPractice/current/?user=" + UserInstance.account.id);

		yield return new WaitForSeconds(2);

		someRequest.Send((request) =>
		{
			if (request.response.status == 200 || request.response.status == 201)
			{
				UserInstance.setCurrentProgramPractice(request.response.Text);
				if (UserInstance.currentProgramPractice != null)
				{
					btnCurrentPractice.SetActive(true);
					_toast.info("Practica pendiente: " + UserInstance.currentProgramPractice.title);
				}
			}
			else
			{
				_toast.warning("VERIFIQUE LA CONEXIÓN A INTERNET--");
			}
		});

		yield return null;
	}

	public void showCurrentPractices()
	{
		mainPanel.SetActive(false);
		currentPracticesPanel.SetActive(true);
	}

	public void startPractice()
	{
		LEVEL.setCurrentLessons(UserInstance.currentProgramPractice.lesson, false);
		LEVEL.is_custom_lesson = false;
		SceneManager.LoadScene("level");
	}

	public void logout()
	{
		mainPanel.SetActive(false);
		panels.SetActive(false);
		loginPanel.SetActive(true);

		inputPassword.text = "";
		inputUsername.text = "";

		UserInstance.setLogin(false);
		UserInstance.account = null;
	}

	public void exit()
	{
		Application.Quit();
	}
}