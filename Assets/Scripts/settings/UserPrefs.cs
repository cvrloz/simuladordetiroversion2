﻿using UnityEngine;
using Scripts;

namespace Assets.Scripts.settings
{
	class UserPrefs
	{
		public static string KEY_FIRST_INIT = "first_init";
		public static string KEY_GENERAL_VOLUME = "general_volume";
		public static string KEY_MUSIC_ENABLED = "music_enabled";

		public static void init()
		{
			if (isFirstInit())
			{
				PlayerPrefs.SetString(KEY_FIRST_INIT, "Y");
				setDefaultPreferences();
				loadPreferences();
			}
		}

		public static void loadPreferences()
		{
			VARS.GENERAL_VOLUME = PlayerPrefs.GetInt(KEY_GENERAL_VOLUME);
			VARS.MUSIC_ENABLED = PlayerPrefs.GetInt(KEY_MUSIC_ENABLED);
		}

		public static bool isFirstInit()
		{
			return PlayerPrefs.GetString(KEY_FIRST_INIT).Equals("");
		}

		public static void setDefaultPreferences()
		{
			PlayerPrefs.SetInt(KEY_GENERAL_VOLUME, 10);
			PlayerPrefs.SetInt(KEY_MUSIC_ENABLED, 1);
		}
	}
}