﻿using System;
using Scripts;
using UnityEngine;
using UnityEngine.UI;
using WiimoteApi;

public class calibration : MonoBehaviour
{
	public RectTransform crossHair;
	public RectTransform _canvas;
	public GameObject panelFinish;
	public GameObject panelInfo;
	public GameObject panelInfoStatus;

	private const int START = 0;
	private const int TOP_CENTER_1 = 1;
	private const int BUTTON_CENTER_1 = 2;
	private const int LEFT_CENTER_1 = 3;
	private const int RIGTH_CENTER_1 = 4;
	private const int FINISH_1 = 5;

	private const int START_2 = 6;
	private const int TOP_CENTER_2 = 7;
	private const int BUTTON_CENTER_2 = 8;
	private const int LEFT_CENTER_2 = 9;
	private const int RIGTH_CENTER_2 = 10;
	private const int FINISH_2 = 11;

	private float top_center = 1;
	private float button_center;
	private float left_center = -1;
	private float rigth_center;

	private bool pressed;
	private Wiimote wiimote;

	private float x;
	private float y;

	private int countPresed;
	private string infoText = "";
	private int current_distance_ir = 1;

	void Start()
	{
		WiimoteManager.FindWiimotes();
		pressed = false;
		countPresed = 0;
		panelInfo.SetActive(true);
		panelFinish.SetActive(false);
		infoText = " -- Dispare para iniciar calibración --";
	}

	void Update()
	{
		if (countPresed > FINISH_2)
		{
			panelFinish.SetActive(true);
			panelInfo.SetActive(false);
			return;
		}

		if (!WiimoteManager.HasWiimote())
		{
			return;
		}

		PlayerPrefs.SetString(VARS.RECT_CALIBRATION, _canvas.rect.width + "&" + _canvas.rect.height);
		wiimote = WiimoteManager.Wiimotes[0];

		wiimote.SendPlayerLED(false, true, false, false);

		while (wiimote.ReadWiimoteData() > 0) ;
		wiimote.SetupIRCamera();

		float[] pointer = wiimote.Ir.GetPointingPosition();
		float distance_ir = Math.Abs(wiimote.Ir.GetProbableSensorBarIR()[0, 0] - wiimote.Ir.GetProbableSensorBarIR()[1, 0]);

		x = -pointer[1];
		y = pointer[0];

		SetPanelStatus(GetPositionCode(distance_ir));

		current_distance_ir = countPresed <= FINISH_1 ? 1 : 2;

		if (wiimote.Button.b && !pressed)
		{
			pressed = true;
			Debug.Log(countPresed);
			switch (countPresed)
			{
				case START:
					panelInfo.GetComponentInChildren<Text>().text =
						"- Inicie disparos desde: \n\t\t-> Arriba\n\t\t-> Abajo\n\t\t-> Izquierda\n\t\t-> Derecha";
					countPresed++;
					break;
				case FINISH_1:
					panelInfo.GetComponentInChildren<Text>().text = "-- Dispare para continuar --";
					countPresed++;
					break;
				case START_2:
					panelInfo.GetComponentInChildren<Text>().text =
						"- Inicie disparos desde: \n\t\t-> Arriba\n\t\t-> Abajo\n\t\t-> Izquierda\n\t\t-> Derecha";
					countPresed++;
					break;
				case FINISH_2:
					panelInfo.GetComponentInChildren<Text>().text = "-- Dispare para finalizar --";
					countPresed++;
					break;
				default:
					if (GetPositionCode(distance_ir) == current_distance_ir)
					{
						panelInfo.GetComponentInChildren<Text>().text = "- Inicie disparos desde: \n" + SetPresedCalibration();
					}
					break;
			}
		}

		pressed = wiimote.Button.b;

		top_center = PlayerPrefs.GetFloat("TOP_" + GetPositionCode(distance_ir), 0);
		button_center = PlayerPrefs.GetFloat("BUTTON_" + GetPositionCode(distance_ir), 1);
		left_center = PlayerPrefs.GetFloat("LEFT_" + GetPositionCode(distance_ir), -1);
		rigth_center = PlayerPrefs.GetFloat("RIGTH_" + GetPositionCode(distance_ir), 0);

		x = Map(x, left_center, rigth_center, -_canvas.rect.width/2, _canvas.rect.width/2);
		y = Map(y, button_center, top_center, -_canvas.rect.height/2, _canvas.rect.height/2);

		try
		{
			crossHair.localPosition = new Vector2(x, y);
		}
		catch (Exception err)
		{
			crossHair.localPosition = new Vector2(0, 0);
		}
	}


	private String SetPresedCalibration()
	{
		String statusPresedPositions = "";
		switch (countPresed)
		{
			case TOP_CENTER_1:
				PlayerPrefs.SetFloat("TOP_1", y);
				statusPresedPositions = "\t\t-> Arriba ◄\n\t\t-> Abajo\n\t\t-> Izquierda\n\t\t-> Deracha\n";
				break;
			case BUTTON_CENTER_1:
				PlayerPrefs.SetFloat("BUTTON_1", y);
				statusPresedPositions = "\t\t-> Arriba ◄\n\t\t-> Abajo ◄\n\t\t-> Izquierda\n\t\t-> Deracha\n";
				break;
			case LEFT_CENTER_1:
				PlayerPrefs.SetFloat("LEFT_1", x);
				statusPresedPositions = "\t\t-> Arriba ◄\n\t\t-> Abajo ◄\n\t\t-> Izquierda ◄\n\t\t-> Deracha\n";
				break;
			case RIGTH_CENTER_1:
				PlayerPrefs.SetFloat("RIGTH_1", x);
				statusPresedPositions =
					"\t\t-> Arriba ◄\n\t\t-> Abajo ◄\n\t\t-> Izquierda ◄\n\t\t-> Deracha ◄\n\t\t\t-- Correcto! --";
				break;
			case TOP_CENTER_2:
				PlayerPrefs.SetFloat("TOP_2", y);
				statusPresedPositions = "\t\t-> Arriba ◄\n\t\t-> Abajo\n\t\t-> Izquierda\n\t\t-> Deracha\n";
				break;
			case BUTTON_CENTER_2:
				PlayerPrefs.SetFloat("BUTTON_2", y);
				statusPresedPositions = "\t\t-> Arriba ◄\n\t\t-> Abajo ◄\n\t\t-> Izquierda\n\t\t-> Deracha\n";
				break;
			case LEFT_CENTER_2:
				PlayerPrefs.SetFloat("LEFT_2", x);
				statusPresedPositions = "\t\t-> Arriba ◄\n\t\t-> Abajo ◄\n\t\t-> Izquierda ◄\n\t\t-> Deracha\n";
				break;
			case RIGTH_CENTER_2:
				PlayerPrefs.SetFloat("RIGTH_2", x);
				statusPresedPositions =
					"\t\t-> Arriba ◄\n\t\t-> Abajo ◄\n\t\t-> Izquierda ◄\n\t\t-> Deracha ◄\n\t\t\t-- Correcto! --";
				break;
		}
		if ((countPresed >= TOP_CENTER_1 && countPresed <= RIGTH_CENTER_1) ||
		    (countPresed >= TOP_CENTER_2 && countPresed <= RIGTH_CENTER_2))
		{
			countPresed++;
		}
		return statusPresedPositions;
	}

	private void SetPanelStatus(int code)
	{
		if (countPresed == START || countPresed == START_2 || countPresed == FINISH_2)
		{
			panelInfoStatus.GetComponent<Image>().color = new Color(55f/255f, 55f/255f, 163f/255f); // blue
			return;
		}
		panelInfoStatus.GetComponent<Image>().color = code == current_distance_ir
			? new Color(0, 141f/255f, 42f/255f)
			: new Color(218f/255f, 50/255f, 50/255f);
	}

	public static int GetPositionCode(float distanceIr)
	{
		if (distanceIr > 100f && distanceIr < 200f) // aprox 2 mt
		{
			return 2;
		}
		if (distanceIr >= 200f && distanceIr <= 400f) // aprox 1 mt
		{
			return 1;
		}
		return 0;
	}

	public static float Map(float point, float minStart, float maxStart, float minEnd, float maxEnd)
	{
		maxStart -= minStart;
		point -= minStart;

		float offsetEnd = minEnd;
		maxEnd -= minEnd;

		float percentPosition = point*100f/maxStart;
		return percentPosition*maxEnd/100f + offsetEnd;
	}

	public static bool ValidateCalibration(RectTransform canvas)
	{
		return PlayerPrefs.GetString(VARS.RECT_CALIBRATION).Equals(canvas.rect.width + "&" + canvas.rect.height);
	}

	public static bool ValidatePosition(float distance_ir)
	{
		return distance_ir > 100f && distance_ir < 400f;
	}
}