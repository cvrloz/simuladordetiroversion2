﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
	void Start()
	{
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene("mainScreen");
		}
	}

	public void restartCalibration()
	{
		SceneManager.LoadScene("Calibration");
	}

	public void returnMain()
	{
		SceneManager.LoadScene("mainScreen");
	}
}