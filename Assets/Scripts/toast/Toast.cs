﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.toast
{
	class Toast : MonoBehaviour
	{
		private GameObject panel = null;
		private float delay = 3f;
		private bool runing = false;
		private float startTime = 0;

		public Toast(GameObject panel)
		{
			this.setPanel(panel);
		}

		public void Update()
		{
			if (runing == true)
			{
				if (Time.time - startTime > delay)
				{
					runing = false;
					panel.GetComponentInChildren<Animator>().Play("message_out");
				}
			}
		}

		public void setPanel(GameObject panel)
		{
			this.panel = panel;
		}

		private void showMessage(String message, Color color)
		{
			if (panel != null)
			{
				startTime = Time.time;
				runing = true;
				panel.GetComponentInChildren<Text>().text = message;
				panel.GetComponentInChildren<Animator>().Play("message_in");
				panel.GetComponent<Image>().color = color;
			}
		}

		public void success(String message)
		{
			this.showMessage(message, new Color(4/255f, 122/255f, 78/255f));
		}

		public void error(String message)
		{
			this.showMessage(message, new Color(156/255f, 11/255f, 32/255f));
		}

		public void warning(String message)
		{
			this.showMessage(message, new Color(114/255f, 111/255f, 0/255f));
		}

		public void info(String message)
		{
			this.showMessage(message, new Color(34/255f, 78/255f, 99/255f));
		}
	}
}