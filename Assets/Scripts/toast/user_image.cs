﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.toast
{
	class user_image : MonoBehaviour
	{
		private string url = "http://127.0.0.1:8000/media/posicion_pie.jpg";
		private Image mg;

		void Start()
		{
			mg = GetComponent<Image>();
			StartCoroutine(loadImage());
		}

		IEnumerator loadImage()
		{
			WWW www = new WWW(url);
			yield return www;
			if (www.texture != null)
			{
				Sprite sprite = new Sprite();
				sprite = Sprite.Create(www.texture, new Rect(0, 0, 100, 100), Vector2.zero, 1);
				mg.sprite = sprite;
			}

			yield return null;
		}
	}
}