﻿using UnityEngine;
using UnityEngine.UI;
using Scripts;

public class SettingManager : MonoBehaviour
{
	public GameObject panel;
	public InputField serverName;
	public Text serverNameText;
	public GameObject mainPanel;
	public Slider volumeSlider;

	void Start()
	{
		panel.SetActive(false);
		volumeSlider.value = VARS.GENERAL_VOLUME;
		serverNameText.text = VARS.BASE_API_URL;
	}

	void Update()
	{
		serverNameText.text = VARS.BASE_API_URL;
	}

	public void activatePanel()
	{
		panel.SetActive(true);
		mainPanel.SetActive(false);
		volumeSlider.value = VARS.GENERAL_VOLUME;
		serverName.text = VARS.BASE_API_URL;
	}

	public void setVolume()
	{
		VARS.GENERAL_VOLUME = volumeSlider.value;
	}

	public void save()
	{
		setServerName();
		setVolume();
		panel.SetActive(false);
		mainPanel.SetActive(true);
	}

	public void setServerName()
	{
		VARS.BASE_API_URL = serverName.text;
	}
}