﻿using UnityEngine;
using System.Collections;
using Scripts;
using UnityEngine.UI;
using System;
using Assets.Scripts.level;
using Assets.Scripts.serializers;
using UnityEngine.SceneManagement;

public class GetLessonList : MonoBehaviour
{
	public RectTransform lessonsPanel;
	private JSONObject results;

	// Use this for initialization
	void OnEnable()
	{
		StartCoroutine(getLessons());
	}

	void OnDisable()
	{
		foreach (Transform childTransform in lessonsPanel.transform)
		{
			Destroy(childTransform.gameObject);
		}
	}

	IEnumerator getLessons()
	{
		// closure necesario sino no mantiene las variables de la iteracion ni con var aux
		Action<JSONObject> create = new Action<JSONObject>((JSONObject j) =>
		{
			string name = j.GetField("name").str;
			GameObject button = (GameObject) Instantiate(Resources.Load("Button1"));			
			button.transform.SetParent(lessonsPanel, false);
			button.transform.localScale = new Vector3(1, 1, 1);
			Button b = button.GetComponent<Button>();
			Text t = b.GetComponentInChildren<Text>();
			t.GetComponent<Text>().color = new Color(1, 1, 1);
			t.text = name;
			b.onClick.AddListener(() => clicked(j));
		});

		HTTP.Request someRequest = new HTTP.Request("get", VARS.BASE_API_URL + "Lesson/?is_complete_serializer=1");
		someRequest.Send();
		while (!someRequest.isDone)
		{
			yield return null;
		}

		JSONObject json = new JSONObject(someRequest.response.Text);
		results = json.GetField("results");
		for (int i = 0; i < results.list.Count; ++i)
		{
			create(results.list[i]);
		}
		yield return null;
	}

	void clicked(JSONObject p)
	{
		Lesson[] lesson = new Lesson[1];
		lesson[0] = new Lesson(p);
		newPractice(lesson);
	}

	public void newPractice(Lesson[] lessons)
	{
		LEVEL.setCurrentLessons(lessons, true);
		LEVEL.indexCurrentTypeOfFire = 0;
		LEVEL.indexCurrentLesson = 0;
		LEVEL.is_custom_lesson = true;
		SceneManager.LoadScene("level");
	}
}