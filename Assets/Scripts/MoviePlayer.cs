﻿using UnityEngine;
using UnityEngine.UI;

public class MoviePlayer : MonoBehaviour
{
	// Use this for initialization

	void Start()
	{
		((MovieTexture) GetComponent<RawImage>().texture).loop = true;
		((MovieTexture) GetComponent<RawImage>().texture).Play();
	}

	// Update is called once per frame
	void Update()
	{
	}
}