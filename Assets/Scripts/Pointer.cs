﻿using UnityEngine;
using WiimoteApi;

public class Pointer : MonoBehaviour
{
	public RectTransform irPointer;
	private Quaternion initial_rotation;
	private Wiimote wiimote;

	private Vector2 scrollPosition;
	private Vector3 wmpOffset = Vector3.zero;

	void Start()
	{
		WiimoteManager.FindWiimotes();
	}

	void Update()
	{
		if (!WiimoteManager.HasWiimote())
		{
			return;
		}

		wiimote = WiimoteManager.Wiimotes[0];
		int ret;
		do
		{
			ret = wiimote.ReadWiimoteData();
			if (ret > 0 && wiimote.current_ext == ExtensionController.MOTIONPLUS)
			{
				Vector3 offset = new Vector3(-wiimote.MotionPlus.PitchSpeed,
					wiimote.MotionPlus.YawSpeed,
					wiimote.MotionPlus.RollSpeed)/95f; // Divide by 95Hz (average updates per second from wiimote)
				wmpOffset += offset;
			}
		} while (ret > 0);

		wiimote.SetupIRCamera(IRDataType.BASIC);

		float maxi = 1000f;
		float[,] ir = wiimote.Ir.GetProbableSensorBarIR();
		float[] pointer = wiimote.Ir.GetPointingPosition();
//        Debug.Log("[" + (1f-ir[0, 0] / maxi) + "] [" + ir[0, 1] / maxi + "]");
		//        Debug.Log("[" + pointer[0] + "] [" + pointer[0] + "]");
//                irPointer.anchorMin = new Vector2(pointer[1], pointer[0]);
//                irPointer.anchorMax = new Vector2(pointer[0], pointer[1]);

		irPointer.position = new Vector3(pointer[1], pointer[0], 0f);

//        irPointer.anchorMin = new Vector2(1f - ir[0, 0] / maxi, 1f - ir[0, 1] / maxi);
//        irPointer.anchorMax = new Vector2(1f - ir[0, 0] / maxi, 1f - ir[0, 1] / maxi);

		if (wiimote.Button.b)
		{
			Debug.Log("Boton");
		}

		//Camera camera = GetComponent<Camera>();
		//camera.ViewportToWorldPoint(new Vector3(pointer[0], pointer[1], 5));
	}
}