﻿using System;
using UnityEngine;
using UnityEngine.UI;
using WiimoteApi;

public class Shoot : MonoBehaviour
{	
	public LevelManager levelManager;
	public GameObject bullet;
    private MenuBehave menu;

	private Camera camera;
	private RaycastHit hit;
	private bool pressed = false;
	private AudioSource shootingSound;
	private Wiimote wiimote;
	private float x;
	private float y;

    public float offsetX;
    public float offsetY;

	public RectTransform _canvas;
	public GameObject point;

	private float top_center = 1;
	private float button_center = 0;
	private float left_center = -1;
	private float rigth_center = 0;


	void Start()
	{
		levelManager = gameObject.GetComponent<LevelManager>();
        point.GetComponent<Image>().color = new Color(3f,3f,3f,255f);
        menu = GetComponent<MenuBehave>();
		camera = GetComponent<Camera>();
		WiimoteManager.FindWiimotes();
        wiimote = WiimoteManager.Wiimotes[0];
        wiimote.SendPlayerLED(true, false, false, false);
		shootingSound = GetComponent<AudioSource>();
		if (!calibration.ValidateCalibration(_canvas))
		{
			Debug.LogWarning("Calibracion realizada con dimensines de pantalla distintos a la actual");
		}
	}

	void Update()
	{
		if (!WiimoteManager.HasWiimote())
		{
            WiimoteManager.FindWiimotes();
            wiimote = WiimoteManager.Wiimotes[0];
            wiimote.SendPlayerLED(true, false, false, false);            
			return;
		}        	

		while (wiimote.ReadWiimoteData() > 0) ;

		wiimote.SetupIRCamera();
		float[] pointer = wiimote.Ir.GetPointingPosition();
		float distance_ir = Math.Abs(wiimote.Ir.GetProbableSensorBarIR()[0, 0] - wiimote.Ir.GetProbableSensorBarIR()[1, 0]);

		menu.timerPanel.GetComponent<Image>().color = calibration.ValidatePosition(distance_ir)
			? new Color(0/255f, 59f/255f, 101f/255f, 100f/255f)
			: new Color(244f/255f, 25f/255f, 25f/255f, 100f/255f);

        x = -pointer[1];
        y = pointer[0];
        Debug.Log("Distancia: "+ distance_ir);
        //Debug.Log(calibration.GetPositionCode(distance_ir));
		top_center = PlayerPrefs.GetFloat("TOP_" + calibration.GetPositionCode(distance_ir), 0);
		button_center = PlayerPrefs.GetFloat("BUTTON_" + calibration.GetPositionCode(distance_ir), 1);
		left_center = PlayerPrefs.GetFloat("LEFT_" + calibration.GetPositionCode(distance_ir), -1);
		rigth_center = PlayerPrefs.GetFloat("RIGTH_" + calibration.GetPositionCode(distance_ir), 0);

        x = calibration.Map(x, left_center, rigth_center, -_canvas.rect.width / 2, _canvas.rect.width / 2) + offsetX;
		y = calibration.Map(y, button_center, top_center, -_canvas.rect.height/2, _canvas.rect.height/2)+offsetY;

		point.transform.localPosition = new Vector2(x, y);
		Vector3 pointNormalized = new Vector3(x + _canvas.rect.width/ 2, y + _canvas.rect.height/2, 0);
		Ray ray = camera.ScreenPointToRay(pointNormalized);

			//Debug.DrawRay(ray.origin, ray.direction*100, Color.yellow);

			if (wiimote.Button.b && Physics.Raycast(ray, out hit) && !pressed)
			{
                SimulatorStatus.ammo--;
                GameObject b = (GameObject)Instantiate(bullet, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
                b.transform.parent = hit.transform.parent;
                hit.transform.root.SendMessage("die");            
                //Destroy(b, 2f);
				try
				{                    
                    SimulatorStatus.score = SimulatorStatus.score + int.Parse(hit.transform.name);
					levelManager.updateScore(int.Parse(hit.transform.name));
				}
				catch (Exception)
				{
					levelManager.updateScore(0);
				}

				Debug.DrawLine(ray.origin, hit.point, Color.cyan, 15, true);
				shootingSound.Play();
				pressed = true;
			}
			pressed = wiimote.Button.b;
		}

}