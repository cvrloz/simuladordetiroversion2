using UnityEngine;

public class positionTiroController : MonoBehaviour
{
	private const int POSITION_STANDING = 1; //de pie
	private const int POSITION_CROUCHING = 2; //agacapado
	private const int POSITION_PRONE = 3; //tendido
	private const int POSITION_KNEE = 4; //rodilla

	private const int BONE_HIP_CENTER = 0;
	private const int BONE_SPINE = 1;
	private const int BONE_SHOULDER_CENTER = 2;
	private const int BONE_HEAD = 3;
	private const int BONE_SHOULDER_LEFT = 4;
	private const int BONE_ELBOW_LEFT = 5;
	private const int BONE_WRIST_LEFT = 6;
	private const int BONE_HAND_LEFT = 7;
	private const int BONE_SHOULDER_RIGHT = 8;
	private const int BONE_ELBOW_RIGHT = 9;
	private const int BONE_WRIST_RIGHT = 10;
	private const int BONE_HAND_RIGHT = 11;
	private const int BONE_HIP_LEFT = 12;
	private const int BONE_KNEE_LEFT = 13;
	private const int BONE_ANKLE_LEFT = 14;
	private const int BONE_FOOT_LEFT = 15;
	private const int BONE_HIP_RIGHT = 16;
	private const int BONE_KNEE_RIGHT = 17;
	private const int BONE_ANKLE_RIGHT = 18;
	private const int BONE_FOOT_RIGHT = 19;

	public GameObject positionObject = null;
	public Texture2D[] texturesPosition;

	public bool MoveVertically = false;
	public bool MirroredMovement = false;

	//public GameObject debugText;

	public GameObject Hip_Center;
	public GameObject Spine;
	public GameObject Shoulder_Center;
	public GameObject Head;
	public GameObject Shoulder_Left;
	public GameObject Elbow_Left;
	public GameObject Wrist_Left;
	public GameObject Hand_Left;
	public GameObject Shoulder_Right;
	public GameObject Elbow_Right;
	public GameObject Wrist_Right;
	public GameObject Hand_Right;
	public GameObject Hip_Left;
	public GameObject Knee_Left;
	public GameObject Ankle_Left;
	public GameObject Foot_Left;
	public GameObject Hip_Right;
	public GameObject Knee_Right;
	public GameObject Ankle_Right;
	public GameObject Foot_Right;

	public LineRenderer SkeletonLine;

	private GameObject[] bones;
	private LineRenderer[] lines;
	private int[] parIdxs;

	private Vector3 initialPosition;
	private Quaternion initialRotation;
	private Vector3 initialPosOffset = Vector3.zero;
	private uint initialPosUserID = 0;


	void Start()
	{
		bones = new GameObject[]
		{
			Hip_Center, Spine, Shoulder_Center, Head, // 0 - 3
			Shoulder_Left, Elbow_Left, Wrist_Left, Hand_Left, // 4 - 7
			Shoulder_Right, Elbow_Right, Wrist_Right, Hand_Right, // 8 - 11
			Hip_Left, Knee_Left, Ankle_Left, Foot_Left, // 12 - 15
			Hip_Right, Knee_Right, Ankle_Right, Foot_Right // 16 - 19
		};

		parIdxs = new int[] {0, 0, 1, 2, 2, 4, 5, 6, 2, 8, 9, 10, 0, 12, 13, 14, 0, 16, 17, 18};

		lines = new LineRenderer[bones.Length];
		if (SkeletonLine)
		{
			for (int i = 0; i < lines.Length; i++)
			{
				lines[i] = Instantiate(SkeletonLine) as LineRenderer;
				lines[i].transform.parent = transform;
			}
		}

		initialPosition = transform.position;
		initialRotation = transform.rotation;
	}

	void changeTexture2D(Texture2D texture)
	{
		positionObject.GetComponent<MeshRenderer>().material.mainTexture = texture;
	}

	void setManTexturePositionPreview(int positionKey)
	{
		switch (positionKey)
		{
			case POSITION_STANDING:
				changeTexture2D(texturesPosition[0]);
				break;
			case POSITION_CROUCHING:
				changeTexture2D(texturesPosition[1]);
				break;
			case POSITION_PRONE:
				changeTexture2D(texturesPosition[2]);
				break;
			case POSITION_KNEE:
				changeTexture2D(texturesPosition[3]);
				break;
		}
	}

	void analizePosition(GameObject[] bonesPosition)
	{
//        Quaternion rotationBone;
		Quaternion rotationShoulerLeft;
		Quaternion rotationShoulerRight;

		rotationShoulerLeft = new Quaternion(
			bonesPosition[BONE_SHOULDER_LEFT].transform.rotation.w,
			bonesPosition[BONE_SHOULDER_LEFT].transform.rotation.x,
			bonesPosition[BONE_SHOULDER_LEFT].transform.rotation.y,
			bonesPosition[BONE_SHOULDER_LEFT].transform.rotation.z);

		rotationShoulerRight = new Quaternion(
			bonesPosition[BONE_SHOULDER_RIGHT].transform.rotation.w,
			bonesPosition[BONE_SHOULDER_RIGHT].transform.rotation.x,
			bonesPosition[BONE_SHOULDER_RIGHT].transform.rotation.y,
			bonesPosition[BONE_SHOULDER_RIGHT].transform.rotation.z);


		Debug.Log(rotationShoulerLeft.eulerAngles);
		Debug.Log(rotationShoulerRight.eulerAngles);

		//positionObject.transform.rotation = rotationShoulerLeft;
		if (
			(rotationShoulerLeft.eulerAngles.x > 330f) &&
			(rotationShoulerRight.eulerAngles.x > 315f || rotationShoulerRight.eulerAngles.x < 45f)
			)
		{
			setManTexturePositionPreview(POSITION_KNEE);
		}
		else
		{
			setManTexturePositionPreview(POSITION_CROUCHING);
		}
		/*for (int i = 0; i < bonesPosition.Length; i++) {
            if (i == BONE_SHOULDER_RIGHT) {

                rotationBone = new Quaternion(bonesPosition[i].transform.rotation.w,
                        bonesPosition[i].transform.rotation.x,
                        bonesPosition[i].transform.rotation.y,
                        bonesPosition[i].transform.rotation.z);
                Debug.Log(rotationBone.eulerAngles);
                positionObject.transform.rotation = rotationBone;
                if (rotationBone.eulerAngles.x < 350) {
                    setManTexturePositionPreview(POSITION_KNEE);
                } else {
                    setManTexturePositionPreview(POSITION_CROUCHING);
                }
            }
        }*/
	}

	void Update()
	{
		/*if (Input.GetKey(KeyCode.DownArrow)) {
            Debug.Log("Presionado A");
            setManPosition(POSITION_KNEE);
        }
        if (Input.GetKey(KeyCode.UpArrow)) {
            Debug.Log("Presionado B");
            setManPosition(POSITION_CROUCHING);
        }*/


		KinectManager manager = KinectManager.Instance;
		uint playerID = manager != null ? manager.GetPlayer1ID() : 0;

		if (playerID <= 0)
		{
			return;
		}
		// set the user position in space
		Vector3 posPointMan = manager.GetUserPosition(playerID);
		//posPointMan.z = !MirroredMovement ? -posPointMan.z : posPointMan.z;

		// store the initial position
		if (initialPosUserID != playerID)
		{
			initialPosUserID = playerID;
			initialPosOffset = transform.position - (MoveVertically ? posPointMan : new Vector3(posPointMan.x, 0, posPointMan.z));
		}

		transform.position = initialPosOffset + (MoveVertically ? posPointMan : new Vector3(posPointMan.x, 0, posPointMan.z));

		for (int i = 0; i < bones.Length; i++)
		{
			if (bones[i] != null)
			{
				int joint = MirroredMovement ? KinectWrapper.GetSkeletonMirroredJoint(i) : i;

				if (manager.IsJointTracked(playerID, joint))
				{
					bones[i].gameObject.SetActive(true);

					Vector3 posJoint = manager.GetJointPosition(playerID, joint);
					posJoint.z = !MirroredMovement ? -posJoint.z : posJoint.z;

					Quaternion rotJoint = manager.GetJointOrientation(playerID, joint, !MirroredMovement);
					rotJoint = initialRotation*rotJoint;

					posJoint -= posPointMan;

					if (MirroredMovement)
					{
						posJoint.x = -posJoint.x;
						posJoint.z = -posJoint.z;
					}

					bones[i].transform.localPosition = posJoint;
					bones[i].transform.rotation = rotJoint;

					analizePosition(bones);
				}
				else
				{
					bones[i].gameObject.SetActive(false);
				}
			}
		}
	}
}