﻿using UnityEngine;
using Assets.Scripts.serializers;

public class RequestPractice : MonoBehaviour
{
	public Practices practices;
	public Results results;
	public Practice practice;
	public ResultsZone[] results_zone;
	// Use this for initialization

	void Start()
	{
		Debug.Log(PlayerPrefs.GetInt("mundo"));
		if (PlayerPrefs.GetString("mundo").Equals(""))
		{
			Debug.Log("No guardo nada");
		}
		else
		{
			Debug.Log(PlayerPrefs.GetString("mundo"));
		}
		PlayerPrefs.SetString("mundo", "22");

		results_zone = new ResultsZone[2];
		results_zone[0] = new ResultsZone(2, 4000, 24);
		results_zone[1] = new ResultsZone(1, 4000, 50);

		practice = new Practice(12, 2, 13, results_zone);
		results = new Results(practice);
		practices = new Practices(1, results, 1);
		Debug.Log(practices.GetJsonString());
	}

	// Update is called once per frame
	void Update()
	{
	}
}