﻿using UnityEngine;
using System.Collections;

public class RandomManager : MonoBehaviour {

    private GameObject target;
    int nfil;
    int ncol;    
    private MenuBehave menu;    

    public bool spawn;
    public bool isFinished;
    public bool paused;

    void Awake()
    {
        Application.targetFrameRate = 30;
    }
    
    	
	void Start () {
      spawn = true;      
      menu = gameObject.GetComponent<MenuBehave>();
      StartCoroutine(preStart(5));
      //actualizar datos de gamestatus
      SimulatorStatus.retrieve();        
	}

    void Update()
    {
        pauseGame();
        finishGame();
        
    }
    
    
    IEnumerator preStart(int delay)
    {        
        gameObject.SendMessage("active", false);

        menu.introPanel.SetActive(true);
        menu.introPanelText.text = "Preparado!";
        yield return new WaitForSeconds(2);

        while (delay > 0)
        {
            menu.introPanelText.text = "" + delay;
            delay--;
            yield return new WaitForSeconds(1);
        }

        menu.introPanelText.text = "Comenzar!";
        yield return new WaitForSeconds(2);
        menu.introPanel.SetActive(false);
        menu.infoPanel.SetActive(true);
        menu.bottomPanel.SetActive(false);
        menu.timerPanel.SetActive(true);


        gameObject.SendMessage("active", true);

        //aca se dispara el comportamiento del nivel
        StartCoroutine(set());
        StartCoroutine(menu.timer());
    }
        

    public void finishGame()
    {
        if (!isFinished)
        {
            if (SimulatorStatus.countDownTime < 0)
            {
                StopCoroutine(menu.timer());
                menu.infoPanel.SetActive(false);
                menu.timerPanel.SetActive(false);
                menu.bottomPanel.SetActive(false);
                menu.fscore.text = SimulatorStatus.score.ToString();
                menu.fwinScore.text = "--";
                menu.fdistance.text = "--";
                menu.fammo.text = "--";
                menu.ftime.text = SimulatorStatus.countDownTime.ToString();
                menu.fname.text = PlayerPrefs.GetString("first_name") + " " + PlayerPrefs.GetString("last_name");
                menu.finishPanel.SetActive(true);
                //desactivar input
                isFinished = true;
            }
        }
    }

    

    public void pauseGame()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {            
            paused = togglePause();
            menu.pausePanel.SetActive(paused);
            SendMessage("active", !paused);            
        }
    }

    bool togglePause()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            return (false);
        }
        else
        {
            Time.timeScale = 0f;
            return (true);
        }
    }



    ////////////////////////////////////////////
    //genera blancos aleatoriamente hasta cambiar bool spawn
    IEnumerator set()
    {
        int afil = 11;
        int acol = 4;
        int i = 0;
        //int afil, acol;
        while (spawn)
        {
            nfil = Random.Range(1, 10);
            ncol = Random.Range(0, 3);

            if (nfil != afil && ncol != acol)
            {
                target = (GameObject)Instantiate(Resources.Load("Target4"));
                target.GetComponent<Target>().init(nfil, ncol);
                afil = nfil;
                acol = ncol;
                ++i;
                Destroy(target, 2.5f);
                yield return new WaitForSeconds(2);
            }

        }
        yield return null;
    }


}
