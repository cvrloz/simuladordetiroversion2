﻿using UnityEngine;
using System.Collections;

public class SimulatorStatus: MonoBehaviour {
    public static int score = 0;
    public static int distance = 5;
    public static int winScore = 5;
    public static int ammo = 5;
    public static int maxAmmo = 5;
    public static int position = 1;
    public static int tar = 1;
    public static float time = 100;
    public static float countDownTime = 100;

    //cambia input
    public static bool isMouse = false;
    public static bool activeInput = true;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public static void retrieve()
    {     
        distance = PlayerPrefs.GetInt("distance");
        winScore = PlayerPrefs.GetInt("winScore");
        ammo = maxAmmo = PlayerPrefs.GetInt("ammo");
        position = PlayerPrefs.GetInt("position");
        tar = PlayerPrefs.GetInt("target");
        time = countDownTime = PlayerPrefs.GetFloat("time");
        Debug.Log("retrieved");
    }
}
