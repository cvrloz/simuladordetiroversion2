﻿using UnityEngine;
using System.Collections;

public class InputSwitcher : MonoBehaviour {       
        
    //switch habilita el mouse por true habilita el wii por false    

	// Use this for initialization
	void Start () {                
        active(SimulatorStatus.activeInput);
	}

    public void active(bool s)
    {
        if(s)
        {
            gameObject.GetComponent<Mouse>().enabled = SimulatorStatus.isMouse; ;
            gameObject.GetComponent<Shoot>().enabled = !SimulatorStatus.isMouse; ;
        }
        else
        {
            gameObject.GetComponent<Mouse>().enabled = false;
            gameObject.GetComponent<Shoot>().enabled = false;                    
        }
        
    }

    public void isMouse(bool m)
    {
        SimulatorStatus.isMouse = m;
    }




}
