﻿using UnityEngine;
using System.Collections;
using Scripts;

public class Mouse : MonoBehaviour
{
	public RectTransform crossHair;
    public GameObject bullet;    
	private Camera camera;
	private RaycastHit hit;
	private bool pressed;
	
	public AudioClip destroySound;
	private AudioSource audiosource;    	

	// Use this for initialization
	void Start()
	{
		pressed = false;        
		audiosource = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{
		camera = GetComponent<Camera>();
		Vector3 viewport = camera.ScreenToViewportPoint(Input.mousePosition);
		Ray ray = camera.ViewportPointToRay(viewport);
		crossHair.anchorMin = new Vector2(viewport.x, viewport.y);
		crossHair.anchorMax = new Vector2(viewport.x, viewport.y);
		//Debug.DrawRay(ray.origin, ray.direction*100, Color.yellow);


		if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit) && !pressed)
		{
            SimulatorStatus.ammo--;
            GameObject b = (GameObject)Instantiate(bullet, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            b.transform.parent = hit.transform.parent.parent;
            //le hago saber al blanco que ha sido tocado                     
            try
            {
                hit.transform.root.SendMessage("die");   
				SimulatorStatus.score = SimulatorStatus.score + int.Parse(hit.transform.name);                                
			}
			catch (System.FormatException)
			{
                
			}

            //Debug.DrawLine(ray.origin, hit.point, Color.cyan, 15, true);
            audiosource.clip = destroySound;
            audiosource.Play();
            pressed = true;
		}
		else if (!Input.GetMouseButtonDown(0))
		{
			pressed = false;
		}
	}
}