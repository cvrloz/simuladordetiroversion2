﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuBehave : MonoBehaviour {
    // standard menu level
  

    public Text fscore, fwinScore, fdistance, fname, ftarget, fammo, ftime, fposition;
    public GameObject btnSave;
    public GameObject btnRestart;

    private GameObject target;
    public GameObject infoPanel;
    public GameObject bottomPanel;
    public GameObject introPanel;
    public GameObject finishPanel;    
    public GameObject timerPanel;
    public GameObject pausePanel;

    public Text introPanelText;
    public Text scoreText;
    public Text TimerText;

    //INFOPANEL VARS
    public Text distanceText, timeText, positionText, bulletstext;

	// Use this for initialization
	void Start ()
    {
             
	}

    void Update() 
    {
        updateData();
    }
	
	
    public void updateData()
    {
        distanceText.text = "Distancia: " + SimulatorStatus.distance + "m";
        timeText.text = "Tiempo Limite: " + SimulatorStatus.time / 60 + " min";
        bulletstext.text = "Cartuchos: " + SimulatorStatus.ammo + " / " + SimulatorStatus.maxAmmo;
        positionText.text = "Posición: " + getPosition(SimulatorStatus.position);
        scoreText.text = SimulatorStatus.score.ToString();
    }   

    public void backToMain()
    {
        togglePause();
        SceneManager.LoadScene("mainScreen");
    }

    public IEnumerator timer()
    {
        while (SimulatorStatus.countDownTime >= 0)
        {
            SimulatorStatus.countDownTime -= Time.deltaTime;
            int minutes = Mathf.FloorToInt(SimulatorStatus.countDownTime / 60F);
            int seconds = Mathf.FloorToInt(SimulatorStatus.countDownTime - minutes * 60);
            string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
            TimerText.text = niceTime;
            yield return null;
        }
    }

    bool togglePause()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            return (false);
        }
        else
        {
            Time.timeScale = 0f;
            return (true);
        }
    }

    public void restartLevel()
    {
        SceneManager.LoadScene("RandomLevel");
    }


    public string getPosition(int i)
    {
        string r = "";
        switch (i)
        {
            case 1:
                r = "De pie";
                break;
            case 2:
                r = "Agazapado";
                break;
            case 3:
                r = "Tendido";
                break;
        }
        return r;
    }    
}
