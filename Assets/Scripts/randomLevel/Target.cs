﻿using UnityEngine;
using System.Collections;
using Scripts;

public class Target : MonoBehaviour {

    private Animator anim;
    public int fil;
    public int col;
    public bool dieFlag;
    public float secs;
	
	void Start () {
        dieFlag = false;
        anim = GetComponent<Animator>();	
	}

    public void init(int fil, int col)
    {
        this.fil = fil;
        this.col = col;
        transform.position = VARS.MOVEMENT_GRID_TOP[fil, col];
    }

    public void die() 
    {        
        anim.SetBool("die", true);                
    }


    
}
