﻿using UnityEngine;
using System.Collections;
using WiimoteApi;
using UnityEngine.UI;
public class GunStatus : MonoBehaviour {
    public Wiimote wiimote;    
    public Text gunStatus;

	// Use this for initialization
	void Start () {
        WiimoteManager.FindWiimotes();
        if(WiimoteManager.HasWiimote())
        {
            wiimote = WiimoteManager.Wiimotes[0];
            wiimote.SendPlayerLED(true, false, false, false);
        }        
	}

    void Update ()
    {

        if (WiimoteManager.HasWiimote())
        {            
            gunStatus.text = "CONECTADO";
        }
        else {            
            gunStatus.text = "DESCONECTADO";
            WiimoteManager.FindWiimotes();
            wiimote = WiimoteManager.Wiimotes[0];
            wiimote.SendPlayerLED(true, false, false, false);
        }       
    
    }
}
