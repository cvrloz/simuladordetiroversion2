﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.level;
using Assets.Scripts.serializers;
using Scripts;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	public bool paused;
	public bool isFinished;

	public AudioSource beepSound;
	public AudioSource beepGo;


	private GameObject target;

	//public GameObject partialFinishPanel;

	private MenuBehave menu;
	private List<ResultsZone> result_zone;

	public int COUNT_START_LESSON = 5;
	//List current lessons

	private Lesson[] listCurrentLesson;
	private int indexCurrentLesson = -1;
	private int indexCurrentTypeOfFire = -1;

	private bool is_continue_fire = false;
	private bool is_custom_lesson = true;

	void Awake()
	{
		Application.targetFrameRate = 30;
	}

	void Start()
	{
		menu = gameObject.GetComponent<MenuBehave>();
		listCurrentLesson = LEVEL.getCurrentLessons();
		is_custom_lesson = LEVEL.is_custom_lesson;
		result_zone = new List<ResultsZone>();
		if (listCurrentLesson != null && listCurrentLesson.Length > 0)
		{
			indexCurrentLesson = LEVEL.indexCurrentLesson;
			indexCurrentTypeOfFire = LEVEL.indexCurrentTypeOfFire;
			startLesson(indexCurrentLesson, indexCurrentTypeOfFire);
		}
		else
		{
			//not found
			LEVEL.is_custom_lesson = true;
			is_custom_lesson = LEVEL.is_custom_lesson;
			listCurrentLesson = LEVEL.getDefaultLessons();
			indexCurrentTypeOfFire = 0;
			indexCurrentLesson = 0;
			startLesson(indexCurrentLesson, indexCurrentTypeOfFire);
		}
	}

	void startLesson(int indexLesson, int indexTypeOfFire)
	{
		paused = false;
		isFinished = false;

		SimulatorStatus.score = 0;
		SimulatorStatus.distance = (int) listCurrentLesson[indexLesson].type_of_fire[indexTypeOfFire].distance;
		SimulatorStatus.time = listCurrentLesson[indexLesson].type_of_fire[indexTypeOfFire].max_time;
		SimulatorStatus.countDownTime = SimulatorStatus.time;
		SimulatorStatus.maxAmmo = listCurrentLesson[indexLesson].type_of_fire[indexTypeOfFire].cartridges;
		SimulatorStatus.ammo = SimulatorStatus.maxAmmo;
		SimulatorStatus.winScore = listCurrentLesson[indexLesson].type_of_fire[indexTypeOfFire].min_score;
		SimulatorStatus.tar = listCurrentLesson[indexLesson].type_of_fire[indexTypeOfFire].target.id; // tar is 1, 2, 3
		SimulatorStatus.position = listCurrentLesson[indexLesson].type_of_fire[indexTypeOfFire].position.id;
		StartCoroutine(startGame());
	}

	// Update is called once per frame
	void Update()
	{
		pauseGame();
		finishGame();
	}

	public void finishGame()
	{
		if (!isFinished)
		{
			if (SimulatorStatus.countDownTime < 0 || SimulatorStatus.ammo == 0)
			{
				StopCoroutine(menu.timer());
				menu.infoPanel.SetActive(false);
				menu.timerPanel.SetActive(false);
				menu.bottomPanel.SetActive(false);

				menu.fscore.text = SimulatorStatus.score.ToString();
				menu.fwinScore.text = SimulatorStatus.winScore.ToString();
				menu.fposition.text = menu.getPosition(SimulatorStatus.position);
				menu.ftarget.text = getTarget();
				menu.fdistance.text = SimulatorStatus.distance.ToString() + " mts";
				menu.fammo.text = SimulatorStatus.maxAmmo.ToString();
				menu.ftime.text = SimulatorStatus.countDownTime.ToString();
				menu.fname.text = UserInstance.account == null ? "Default user " : UserInstance.account.getFullName();

				gameObject.SendMessage("active", false);

				isFinished = true;

				//en caso finalizar por sobrepasar el tiempo, llenamos con time=0, score=0

				if (result_zone.Count <
				    listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].cartridges)
				{
					int offset = listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].cartridges -
					             result_zone.Count;
					for (int i = 0; i < offset; i++)
					{
						result_zone.Add(new ResultsZone(0, 0, 0));
					}
				}

				Text txt = menu.btnSave.GetComponentInChildren<Text>();
				txt.text = "Continuar";
				is_continue_fire = true;
				if (indexCurrentTypeOfFire + 1 < listCurrentLesson[indexCurrentLesson].type_of_fire.Length)
				{
					LEVEL.indexCurrentTypeOfFire++;
					menu.btnRestart.SetActive(false);
					LEVEL.practices.addResult(listCurrentLesson[indexCurrentLesson],
						listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire],
						listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].position, result_zone);
					result_zone.Clear();
					menu.finishPanel.SetActive(true);
				}
				else
				{
					LEVEL.indexCurrentTypeOfFire = 0;
					if (indexCurrentLesson + 1 < listCurrentLesson.Length)
					{
						menu.btnRestart.SetActive(false);
						LEVEL.indexCurrentLesson ++;
						LEVEL.practices.addResult(listCurrentLesson[indexCurrentLesson],
							listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire],
							listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].position,
							result_zone);
						result_zone.Clear();
						menu.finishPanel.SetActive(true);
					}
					else
					{
						menu.btnRestart.SetActive(is_custom_lesson);
						txt.text = "Guardar";
						is_continue_fire = false;
						LEVEL.practices.addResult(listCurrentLesson[indexCurrentLesson],
							listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire],
							listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].position,
							result_zone);
						result_zone.Clear();
						LEVEL.indexCurrentLesson = 0;
						menu.finishPanel.SetActive(true);
					}
				}
			}
		}
	}

	public string getPosition()
	{
		return listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].position.name;
	}

	public string getTarget()
	{
		return listCurrentLesson[indexCurrentLesson].type_of_fire[indexCurrentTypeOfFire].target.name;
	}


	public void updateScore(int points)
	{
		result_zone.Add(new ResultsZone(points, 0, points));
		//SimulatorStatus.score = SimulatorStatus.score + points;
	}

	public void restartLevel()
	{
		LEVEL.indexCurrentTypeOfFire = 0;
		LEVEL.indexCurrentLesson = 0;
		Application.LoadLevel(1);
	}

	public void save_or_continue()
	{
		if (is_continue_fire)
		{
			Application.LoadLevel(1);
		}
		else
		{
			if (!is_custom_lesson)
			{
				LEVEL.clear();
			}
			string jsonString = LEVEL.getPracticeJson();

			WWWForm form = new WWWForm();
			Dictionary<string, string> headers = form.headers;
			headers["Content-Type"] = "application/json";
			Hashtable data = new Hashtable();
			string json = jsonString;
			Debug.Log(json);
			byte[] bytes = Encoding.UTF8.GetBytes(json);
			string url;
			if (LEVEL.is_custom_lesson)
			{
				url = VARS.BASE_API_URL + "CustomPractices/";
			}
			else
			{
				url = VARS.BASE_API_URL + "Practices/";
			}
			WWW www = new WWW(url, bytes, headers);
			StartCoroutine(savePractice(www));
		}
	}

	IEnumerator savePractice(WWW www)
	{
		yield return www;
		Debug.Log(www.url);
		if (www.error != null)
		{
			Debug.Log("ERROR");
			Debug.Log(www.error);
		}
		else
		{
			menu.backToMain();
		}
	}

	public GameObject setTarget(int i)
	{
		GameObject r = new GameObject();
		switch (i)
		{
			case 1:
				r = (GameObject) Instantiate(Resources.Load("Target1"));
				break;
			case 2:
				r = (GameObject) Instantiate(Resources.Load("Target2"));
				break;
			case 3:
				r = (GameObject) Instantiate(Resources.Load("Target3"));
				break;
		}
		return r;
	}

	IEnumerator startGame()
	{
		Debug.Log("game started");
		paused = false;
		gameObject.SendMessage("active", false);
		menu.introPanel.SetActive(true);
		menu.introPanelText.text = "Preparado!";
		target = setTarget(SimulatorStatus.tar);
		target.GetComponent<TargetManager>().init(0, 1);
		yield return new WaitForSeconds(2);
		target.GetComponent<TargetManager>().startSpawn(SimulatorStatus.distance / 5);
		yield return new WaitForSeconds(2);

		int c = COUNT_START_LESSON;
		while (c > 0)
		{
			menu.introPanelText.text = "" + c;
			c--;
			beepSound.Play();
			yield return new WaitForSeconds(1);
		}

		beepGo.Play();
		menu.introPanelText.text = "Comenzar!";
		yield return new WaitForSeconds(2);

		menu.introPanel.SetActive(false);
		menu.infoPanel.SetActive(true);
		menu.bottomPanel.SetActive(true);
		menu.timerPanel.SetActive(true);
		gameObject.SendMessage("active", true);
		StartCoroutine(menu.timer());
	}

	public void pauseGame()
	{
		if (Input.GetKeyDown(KeyCode.P))
		{
			paused = togglePause();
			menu.pausePanel.SetActive(paused);
			SendMessage("active", !paused);
		}
	}

	bool togglePause()
	{
		if (Time.timeScale == 0f)
		{
			Time.timeScale = 1f;
			return (false);
		}
		else
		{
			Time.timeScale = 0f;
			return (true);
		}
	}
}