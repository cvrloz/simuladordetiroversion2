using UnityEngine;

namespace Scripts
{
	public static class VARS
	{
		public static string URL_PROGRAM_PRACTICE_CURRENT = "/Current";
		public static float spacing = 5f;
		public static float x = -1.65f;
		public static string RECT_CALIBRATION = "RECT_CALIBRATION";

        /// <summary>
        /// [9,0][9,1][9,2]
        /// [8,0][8,1][8,2]
        /// [7,0][7,1][7,2]
        /// [6,0][6,1][6,2]
        /// [5,0][5,1][5,2]
        /// [4,0][4,1][4,2]
        /// [3,0][3,1][3,2]
        /// [2,0][2,1][2,2]
        /// [1,0][1,1][1,2]
        /// [0,0][0,1][2,2]
        /// </summary>

		public static Vector3[,] MOVEMENT_GRID_TOP = new Vector3[10, 3]
		{
			{
				new Vector3(x - spacing*0, 3.031f, 1.928f), new Vector3(x - spacing*0, 3.031f, 3.87f),
				new Vector3(x - spacing*0, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*1, 3.031f, 1.928f), new Vector3(x - spacing*1, 3.031f, 3.87f),
				new Vector3(x - spacing*1, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*2, 3.031f, 1.928f), new Vector3(x - spacing*2, 3.031f, 3.87f),
				new Vector3(x - spacing*2, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*3, 3.031f, 1.928f), new Vector3(x - spacing*3, 3.031f, 3.87f),
				new Vector3(x - spacing*3, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*4, 3.031f, 1.928f), new Vector3(x - spacing*4, 3.031f, 3.87f),
				new Vector3(x - spacing*4, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*5, 3.031f, 1.928f), new Vector3(x - spacing*5, 3.031f, 3.87f),
				new Vector3(x - spacing*5, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*6, 3.031f, 1.928f), new Vector3(x - spacing*6, 3.031f, 3.87f),
				new Vector3(x - spacing*6, 3.031f, 5.87f)
			}, // 1   5
			{
				new Vector3(x - spacing*7, 3.031f, 1.928f), new Vector3(x - spacing*7, 3.031f, 3.87f),
				new Vector3(x - spacing*7, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*8, 3.031f, 1.928f), new Vector3(x - spacing*8, 3.031f, 3.87f),
				new Vector3(x - spacing*8, 3.031f, 5.87f)
			},
			{
				new Vector3(x - spacing*9, 3.031f, 1.928f), new Vector3(x - spacing*9, 3.031f, 3.87f),
				new Vector3(x - spacing*9, 3.031f, 5.87f)
			}
		};

		public static Vector3[,] MOVEMENT_GRID_FLOOR = new Vector3[8, 3]
		{
			{
				new Vector3(-30.62f + 0*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 0*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 0*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 1*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 1*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 1*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 2*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 2*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 2*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 3*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 3*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 3*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 4*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 4*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 4*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 5*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 5*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 5*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 6*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 6*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 6*spacing, 3.031f, 5.87f)
			},
			{
				new Vector3(-30.62f + 7*spacing, 3.031f, 1.928f), new Vector3(-30.62f + 7*spacing, 3.031f, 3.87f),
				new Vector3(-30.62f + 7*spacing, 3.031f, 5.87f)
			}
		};


		public static int DIANA_10_ZONES = 1;
		public static int DIANA_E = 1;
//		public static string BASE_API_URL = "https://simulador-tiro.herokuapp.com/";
		public static string BASE_API_URL = "http://127.0.0.1:8000/";
		public static float GENERAL_VOLUME = 10;
		public static int MUSIC_ENABLED = 1;
	}
}