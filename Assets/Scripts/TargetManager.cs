﻿using UnityEngine;
using System.Collections;
using Scripts;

public class TargetManager : MonoBehaviour
{	
	float step;
	public float speed;
	public int fil;
	public int col;
	private bool spawned;
    private Animator anim;

	// Use this for initialization
	void Start()
	{
        anim = GetComponent<Animator>();
		spawned = false;
	}

	public void init(int fil, int col)
	{
		this.fil = fil;
		this.col = col;
		transform.position = VARS.MOVEMENT_GRID_TOP[fil, col];
	}


	IEnumerator spawn(int spawnFil)
	{
		Debug.Log("comienza spawn");
		while (!V3Equal(transform.position, VARS.MOVEMENT_GRID_TOP[spawnFil, col]))
		{
			step = speed*Time.deltaTime;
            anim.SetInteger("direction",3);
			transform.position = Vector3.MoveTowards(transform.position, VARS.MOVEMENT_GRID_TOP[spawnFil, col], step);

			yield return new WaitForSeconds(0.05f);
		}        
		Debug.Log("termina spawn");
        //anim.SetInteger("direction", 0);
	}


	public void startSpawn(int spawnFil)
	{
		StartCoroutine(spawn(spawnFil));
	}


	private bool V3Equal(Vector3 a, Vector3 b)
	{
		return Vector3.SqrMagnitude(a - b) <= 1;
	}
}